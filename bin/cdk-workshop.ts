#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { CDKWorkshopStack } from '../lib/cdk-workshop-stack'

// Main entry point to the APP CDKWorkshopStack
const app = new cdk.App();
new CDKWorkshopStack(app, 'CDKWorkshopStack');