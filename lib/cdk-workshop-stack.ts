import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda'
import * as apigw from 'aws-cdk-lib/aws-apigateway'

export class CDKWorkshopStack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
      super(scope, id, props);


    //Utilize lambda function created in /lamda/Hello.ts
    const lambdaHandler = new lambda.Function(this, 'HelloHandler',{
        runtime: lambda.Runtime.NODEJS_18_X, // runtime environment for running lambda function
        code: lambda.Code.fromAsset('lambda'), // code will be loaded from lambda directory
        handler: 'hello.handler' // // file is "Hello", function is "handler"
      })

      // defines an API Gateway REST API resource backed by our "hello" function.
      new apigw.LambdaRestApi(this, "Endpoint",{
          handler: lambdaHandler
      });

    }
}

 
